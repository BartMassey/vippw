# Test Procedure
Copyright (c) 2016 Bart Massey

You must execute and pass this *entire* test procedure
before commiting changes to `vippw` master.

1.  `rm -rf /tmp/vippw-test`
2.  Copy `vippw.sh` to `/tmp/vippw` and `chmod 700 /tmp/vippw`.
3.  Edit `/tmp/vippw` to change `PRODUCTION` to `false`.
4.  `/tmp/vippw` and ensure that a new empty password file
    appears. Should be prompted twice for the initial
    password and then again to edit.
5.  After exiting, ensure that `/tmp/vippw-test` contains
    `passwords.te` and nothing else.
6.  `/tmp/vippw` and edit the file to insert some content.
7.  `/tmp/vippw` and ensure that content was retained.
8.  Exit without inserting content and ensure
    that no password is prompted for.
9.  `/tmp/vippw` and insert some more content.
10. After exiting, ensure that `/tmp/vippw-test` contains
    `passwords.te`, `passwords.te.bak`, and nothing else.
11. `/tmp/vippw` and ensure that the new content was retained.
12. `mv -f /tmp/vippw-test/passwords.te.bak /tmp/vippw-test/passwords.te`
13. `/tmp/vippw` and ensure that the first content is
    present and the second content absent.
14. After exiting, ensure that `/tmp/vippw-test` contains
    `passwords.te` and nothing else.
15. `cp test-passwords.te /tmp/vippw-test/passwords.te`
16. `/tmp/vippw` with password "test" and ensure that the
    edit buffer contains "compatible".
17. `rm -rf /tmp/vippw-test /tmp/vippw`

---

This work is available under the GPL Version 3. Please
see the file `COPYING` in this distribution for license
details.
