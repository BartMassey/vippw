# vippw: Edit encrypted password file
Copyright (c) 2016 Bart Massey

This script is intended to edit an encrypted text file of
personal passwords: I imagine it could be used to edit some
other kind of encrypted text file equally well.

## Usage

When first invoked, the script will prompt twice for
a password. After that, it will prompt again for the
password so that you can edit the file. It will continue
to do that on subsequent invocations.

If you do not edit the file, exiting the editor will simply
exit. If you do edit the file, the script will prompt twice
for the password again and save the changes.

The script will use the environment variable `EDITOR` if
present, and will otherwise invoke `vi`.

## Installation

The script requires `mcrypt` (and `mdecrypt`) to
function. On Debian systems, `mcrypt` can be installed
with `sudo apt-get install mcrypt`.

Copy `vippw.sh` to `vippw` in whatever bin directory you
want it to live in, and then `chmod 555 vippw` and `sudo
chown root vippw`. You don't want some attacker editing
this.

## Modification

If you want to enhance or repair `vippw`, follow the
directions in the file `TESTING.md` in this distribution
before committing changes to Git master.

## License

This work is available under the GPL Version 3. Please
see the file `COPYING` in this distribution for license
details.
