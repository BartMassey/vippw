#!/bin/sh
# Copyright © 2016 Bart Massey
# [This work is available under the GPL Version 3. Please see
# the file COPYING in this distribution for license details.]

# Editor for encrypted password file.

# Directory for passwords should be private, permanent
# storage.
PRODUCTION_PWDIR=$HOME/.vippw

# Use this editor when $EDITOR is unset or empty.
DEFAULT_EDITOR=vi

# Set this true for normal use, false for testing.
# XXX The testing password directory is not a place to have
# your permanent record, so never edit the in-git or
# installed version of this file to set PRODUCTION false:
# make a copy and edit that instead.
PRODUCTION=true

# Choose an appropriate password directory.
if $PRODUCTION
then
    PWDIR=$PRODUCTION_PWDIR
else
    PWDIR=/tmp/vippw-test
fi

# Ensure the existence and permissions of the password directory.
[ -d $PWDIR ] || mkdir $PWDIR || exit 1
chmod 700 $PWDIR

# AES in a mode compatible with the old "des" program.
MCRYPTARGS="-q -a rijndael-256 --keymode pkdes --bare --noiv"

# Original encrypted password file.
OLD_PW=$PWDIR/passwords.te
# Decrypted scratch copy.
TMP_SCRATCH=$PWDIR/passwords-$$.txt
# Decrypted backup scratch copy for comparison.
TMP_SCRATCH_COPY=$PWDIR/passwords-copy-$$.txt
# New encrypted password file.
NEW_PW=$PWDIR/passwords-$$.te
# Backup of old encrypted password file.
BACKUP_PW=$OLD_PW.bak

# Do not remove the backup encrypted passsword file if this
# thing fails.
trap "rm -f $TMP_SCRATCH $TMP_SCRATCH_COPY" 0 1 2 3 15

# Processing logic.

# Start a new password file if needed.
( [ -f $OLD_PW ] || echo "" | mcrypt $MCRYPTARGS > $OLD_PW ) &&
# Decrypt the old password file.
mdecrypt $MCRYPTARGS <$OLD_PW >$TMP_SCRATCH &&
# Make a temporary unencrypted backup.
cp $TMP_SCRATCH $TMP_SCRATCH_COPY &&
# Let the user edit the password file.
${EDITOR:-$DEFAULT_EDITOR} $TMP_SCRATCH &&
# If no changes have been made, clean up and exit.
cmp -s $TMP_SCRATCH $TMP_SCRATCH_COPY && exit 0 ||
# Re-encrypt the password file.
mcrypt $MCRYPTARGS <$TMP_SCRATCH >$NEW_PW &&
# Leave the old encrypted password file as a backup.
mv $OLD_PW $BACKUP_PW &&
# Put the new encrypted password file in place.
mv $NEW_PW $OLD_PW
